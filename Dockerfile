FROM ubuntu:latest

LABEL authors="Sibin Prasad"

RUN apt-get update && apt-get install -y curl git

ENTRYPOINT ["top", "-b"]